package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LightActivity extends AppCompatActivity
{

    ImageView imgLight;
    ImageButton imgSwitch;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    String homeName;
    String roomName;
    String deviceName;
    String status;
    DatabaseReference lightRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);

        imgLight = findViewById(R.id.imgLightDL);
        imgSwitch = findViewById(R.id.imgLightSwitchDL);
        homeName = getIntent().getStringExtra("HOME_NAME");
        roomName = getIntent().getStringExtra("ROOM_NAME");
        deviceName = getIntent().getStringExtra("DEVICE_NAME");
        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null)
        {
            user = mAuth.getCurrentUser();
            userID = user.getUid();
            lightRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes").child(homeName).child("rooms").child(roomName).child("devices").child(deviceName);

            lightRef.addValueEventListener(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.child("status").getValue().equals("ON"))
                    {
                        imgLight.setImageResource(R.drawable.light_on);
                        imgSwitch.setImageResource(R.drawable.switch_on);
                        status = "ON";
                    }
                    else
                    {
                        imgLight.setImageResource(R.drawable.light_off);
                        imgSwitch.setImageResource(R.drawable.switch_off);
                        status = "OFF";
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });


            imgSwitch.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(status.equals("ON"))
                        lightRef.child("status").setValue("OFF");
                    else if(status.equals("OFF"))
                        lightRef.child("status").setValue("ON");
                }
            });


        }
    }
}
