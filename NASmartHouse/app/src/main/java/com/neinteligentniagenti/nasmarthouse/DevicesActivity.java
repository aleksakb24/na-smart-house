package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Locale;

public class DevicesActivity extends AppCompatActivity
{

    TextView txtRoom;
    RecyclerView listDevices;
    FloatingActionButton btnAddDevice,btnSpeech;
    EditText txtDialogDeviceName;
    RadioButton rbtnCamera,rbtnSocket,rbtnLight;

    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    String speechCommand;
    AlertDialog dialog2;

    Button btnDialog;
    String dialogDeviceName;
    String dialogType;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    String homeName;
    String roomName;
    DatabaseReference devicesRef, speechRef, deviceSpeechRef;
    FirebaseRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        checkPermission();


        txtRoom = findViewById(R.id.txtRoomD);
        listDevices = findViewById(R.id.rcvDevices);
        btnAddDevice = findViewById(R.id.fbtnAddDevices);
        btnSpeech = findViewById(R.id.fbtnSpeechD);

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener()
        {
            @Override
            public void onReadyForSpeech(Bundle params)
            {

            }

            @Override
            public void onBeginningOfSpeech()
            {

            }

            @Override
            public void onRmsChanged(float rmsdB)
            {

            }

            @Override
            public void onBufferReceived(byte[] buffer)
            {

            }

            @Override
            public void onEndOfSpeech()
            {
                mSpeechRecognizer.stopListening();
                dialog2.cancel();

            }

            @Override
            public void onError(int error)
            {

            }

            @Override
            public void onResults(Bundle results)
            {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                if(matches != null)
                {
                    speechCommand = matches.get(0).toUpperCase();
                    speechRef = FirebaseDatabase.getInstance().getReference().child("speech");
                    speechRef.setValue(speechCommand);
                    checkVoiceCommand();
                }

            }

            @Override
            public void onPartialResults(Bundle partialResults)
            {

            }

            @Override
            public void onEvent(int eventType, Bundle params)
            {

            }
        });


        homeName = getIntent().getStringExtra("HOME_NAME");
        roomName = getIntent().getStringExtra("ROOM_NAME");
        mAuth = FirebaseAuth.getInstance();

        txtRoom.setText(roomName);

        if(mAuth.getCurrentUser() != null)
        {
            user = mAuth.getCurrentUser();
            userID = user.getUid();
            devicesRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes").child(homeName).child("rooms").child(roomName).child("devices");


            btnAddDevice.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(DevicesActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.add_devices_dialog,null);
                    txtDialogDeviceName = mView.findViewById(R.id.txtDialogDeviceName);
                    rbtnCamera = mView.findViewById(R.id.rbtnCamera);
                    rbtnSocket = mView.findViewById(R.id.rbtnSocket);
                    rbtnLight = mView.findViewById(R.id.rbtnLight);

                    btnDialog = mView.findViewById(R.id.btnDialogAddDevice);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    btnDialog.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            dialogDeviceName = txtDialogDeviceName.getText().toString().toUpperCase().trim();

                            if(rbtnLight.isChecked())
                                dialogType = "Light";
                            else if(rbtnSocket.isChecked())
                                dialogType = "Socket";
                            else if(rbtnCamera.isChecked())
                                dialogType = "Camera";
                            else
                                dialogType = "";

                            if(dialogDeviceName.isEmpty())
                            {
                                txtDialogDeviceName.setError("Please enter a device name.");
                                txtDialogDeviceName.requestFocus();
                            }
                            else if(dialogType.isEmpty())
                            {
                                rbtnCamera.setError("Please select a type.");
                                rbtnCamera.requestFocus();
                            }
                            else
                            {
                                devicesRef.child(dialogDeviceName).child("deviceName").setValue(dialogDeviceName);
                                devicesRef.child(dialogDeviceName).child("status").setValue("OFF");
                                devicesRef.child(dialogDeviceName).child("type").setValue(dialogType).addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        Toast.makeText(getApplicationContext(),"Device added successfully", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                    }
                                });

                            }


                        }
                    });

                }
            });


            btnSpeech.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(DevicesActivity.this);
                    View mView2 = getLayoutInflater().inflate(R.layout.speech_recording_device,null);

                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

                    mBuilder2.setView(mView2);
                    dialog2 = mBuilder2.create();
                    dialog2.show();

                }
            });

            linearLayoutManager = new LinearLayoutManager(this);
            listDevices.setLayoutManager(linearLayoutManager);
            listDevices.setHasFixedSize(true);
            ShowDevices();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    public void ShowDevices()
    {
        Query searchDevices = devicesRef;

        FirebaseRecyclerOptions<DeviceModel> options = new FirebaseRecyclerOptions.Builder<DeviceModel>()
                .setQuery(searchDevices, new SnapshotParser<DeviceModel>()
                {
                    @NonNull
                    @Override
                    public DeviceModel parseSnapshot(@NonNull DataSnapshot snapshot)
                    {
                        return new DeviceModel(snapshot.child("deviceName").getValue().toString(),snapshot.child("status").getValue().toString(),snapshot.child("type").getValue().toString());
                    }
                })
                .build();


        adapter = new FirebaseRecyclerAdapter<DeviceModel, DevicesActivity.DeviceViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull DevicesActivity.DeviceViewHolder deviceViewHolder, int i, @NonNull DeviceModel deviceModel)
            {
                deviceViewHolder.deviceName.setText(deviceModel.getDeviceName());
                deviceViewHolder.deviceStatus.setText(deviceModel.getStatus());

                final String dName = deviceModel.getDeviceName();
                final String dStatus = deviceModel.getStatus();
                final String dType = deviceModel.getType();

                if(dType.equals("Camera"))
                    deviceViewHolder.btnDevice.setImageResource(R.drawable.camera);
                else if(dType.equals("Socket"))
                    deviceViewHolder.btnDevice.setImageResource(R.drawable.socket);
                else if(dType.equals("Light"))
                    deviceViewHolder.btnDevice.setImageResource(R.drawable.light);


                if(dStatus.equals("ON"))
                    deviceViewHolder.imgStatus.setImageResource(R.drawable.status_on);
                else if(dStatus.equals("OFF"))
                    deviceViewHolder.imgStatus.setImageResource(R.drawable.status_off);

                deviceViewHolder.btnDevice.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent deviceIntent = new Intent();

                        if(dType.equals("Camera"))
                            deviceIntent = new Intent(getApplicationContext(),CameraActivity.class);
                        else if(dType.equals("Socket"))
                            deviceIntent = new Intent(getApplicationContext(),SocketActivity.class);
                        else if(dType.equals("Light"))
                            deviceIntent = new Intent(getApplicationContext(),LightActivity.class);


                        deviceIntent.putExtra("HOME_NAME",homeName);
                        deviceIntent.putExtra("ROOM_NAME",roomName);
                        deviceIntent.putExtra("DEVICE_NAME",dName);
                        startActivity(deviceIntent);
                    }
                });

                deviceViewHolder.btnRemove.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        devicesRef.child(dName).removeValue();
                    }
                });
            }

            @NonNull
            @Override
            public DevicesActivity.DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.devices_view_holder,parent,false);
                DevicesActivity.DeviceViewHolder viewHolder = new DevicesActivity.DeviceViewHolder(view);
                return viewHolder;
            }
        };

        listDevices.setAdapter(adapter);
    }


    public static class DeviceViewHolder extends RecyclerView.ViewHolder
    {
        TextView deviceName;
        TextView deviceStatus;
        ImageButton btnDevice;
        ImageView imgStatus;

        ImageButton btnRemove;


        public DeviceViewHolder(View itemView)
        {
            super(itemView);
            deviceName = itemView.findViewById(R.id.txtDeviceNameD);
            deviceStatus = itemView.findViewById(R.id.txtDeviceStatusD);
            btnDevice = itemView.findViewById(R.id.btnDeviceD);
            imgStatus = itemView.findViewById(R.id.imgStatusD);

            btnRemove = itemView.findViewById(R.id.btnRemoveD);

        }

    }


    public void checkPermission()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }


        }
    }


    public void checkVoiceCommand()
    {
        String[] stringArray;
        final String on_off;
        String deviceCommand = "";

        stringArray = speechCommand.split(" ");
        on_off = stringArray[0] + " " + stringArray[1];

        if(on_off.equals("TURN ON") || on_off.equals("TURN OFF"))
        {
            int length = stringArray.length;
            int startInd = 2;

            if(stringArray[2].equals("THE"))
                startInd = 3;

            for(int i = startInd; i < length; i++)
            {
                deviceCommand += stringArray[i];
                if(length - i != 1)
                    deviceCommand += " ";
            }
            final String dc = deviceCommand;
            deviceSpeechRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes").child(homeName).child("rooms").child(roomName).child("devices").child(deviceCommand).child("deviceName");

            deviceSpeechRef.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.getValue() != null)
                    {
                        if(on_off.equals("TURN OFF"))
                            devicesRef.child(dc).child("status").setValue("OFF");
                        else
                            devicesRef.child(dc).child("status").setValue("ON");
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Unknown device", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });



        }
        else
        {
            Toast.makeText(getApplicationContext(),"Unknown command", Toast.LENGTH_SHORT).show();
        }
    }
}
