package com.neinteligentniagenti.nasmarthouse;

public class HomeModel
{
    public String homeName;
    public String address;

    public HomeModel()
    {
    }

    public HomeModel(String homeName, String address)
    {
        this.homeName = homeName;
        this.address = address;
    }

    public String getHomeName() {return homeName;}
    public void setHomeName(String name) {this.homeName = name;}

    public String getAddress() {return address;}
    public void setAddress(String address) {this.address = address;}
}
