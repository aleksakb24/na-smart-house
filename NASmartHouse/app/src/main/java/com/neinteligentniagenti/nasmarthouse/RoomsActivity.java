package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Locale;

public class RoomsActivity extends AppCompatActivity
{

    TextView txtHouse;
    RecyclerView listRooms;
    FloatingActionButton btnAddRooms, btnSpeech;
    EditText txtDialogRoomName;
    Button btnDialog;

    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    String speechCommand;
    AlertDialog dialog2;


    String dialogRoomName;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    String homeName;
    DatabaseReference roomsRef, speechRef, roomSpeechRef;
    FirebaseRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        checkPermission();

        txtHouse = findViewById(R.id.txtHouseR);
        listRooms = findViewById(R.id.rcvRooms);
        btnAddRooms = findViewById(R.id.fbtnAddRooms);
        btnSpeech = findViewById(R.id.fbtnSpeechR);



        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener()
        {
            @Override
            public void onReadyForSpeech(Bundle params)
            {

            }

            @Override
            public void onBeginningOfSpeech()
            {

            }

            @Override
            public void onRmsChanged(float rmsdB)
            {

            }

            @Override
            public void onBufferReceived(byte[] buffer)
            {

            }

            @Override
            public void onEndOfSpeech()
            {
                mSpeechRecognizer.stopListening();
                dialog2.cancel();

            }

            @Override
            public void onError(int error)
            {

            }

            @Override
            public void onResults(Bundle results)
            {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                if(matches != null)
                {
                    speechCommand = matches.get(0).toUpperCase();
                    speechRef = FirebaseDatabase.getInstance().getReference().child("speech");
                    speechRef.setValue(speechCommand);
                    checkVoiceCommand();
                }

            }

            @Override
            public void onPartialResults(Bundle partialResults)
            {

            }

            @Override
            public void onEvent(int eventType, Bundle params)
            {

            }
        });



        homeName = getIntent().getStringExtra("HOME_NAME");
        mAuth = FirebaseAuth.getInstance();

        txtHouse.setText(homeName);

        if(mAuth.getCurrentUser() != null)
        {
            user = mAuth.getCurrentUser();
            userID = user.getUid();
            roomsRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes").child(homeName).child("rooms");


            btnAddRooms.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(RoomsActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.add_rooms_dialog,null);
                    txtDialogRoomName = mView.findViewById(R.id.txtDialogRoomName);
                    btnDialog = mView.findViewById(R.id.btnDialogAddRoom);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    btnDialog.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            dialogRoomName = txtDialogRoomName.getText().toString().toUpperCase().trim();

                            if(dialogRoomName.isEmpty())
                            {
                                txtDialogRoomName.setError("Please enter a room name.");
                                txtDialogRoomName.requestFocus();
                            }
                            else
                            {
                                roomsRef.child(dialogRoomName).child("roomName").setValue(dialogRoomName).addOnSuccessListener(new OnSuccessListener<Void>()
                                {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {
                                        Toast.makeText(getApplicationContext(),"Room added successfully", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();

                                    }
                                });

                            }


                        }
                    });

                }
            });


            btnSpeech.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(RoomsActivity.this);
                    View mView2 = getLayoutInflater().inflate(R.layout.speech_recording_room,null);

                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

                    mBuilder2.setView(mView2);
                    dialog2 = mBuilder2.create();
                    dialog2.show();

                }
            });


            linearLayoutManager = new LinearLayoutManager(this);
            listRooms.setLayoutManager(linearLayoutManager);
            listRooms.setHasFixedSize(true);
            ShowRooms();



        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    public void ShowRooms()
    {
        Query searchRooms = roomsRef;

        FirebaseRecyclerOptions<RoomModel> options = new FirebaseRecyclerOptions.Builder<RoomModel>()
                .setQuery(searchRooms, new SnapshotParser<RoomModel>()
                {
                    @NonNull
                    @Override
                    public RoomModel parseSnapshot(@NonNull DataSnapshot snapshot)
                    {
                        return new RoomModel(snapshot.child("roomName").getValue().toString());
                    }
                })
                .build();


        adapter = new FirebaseRecyclerAdapter<RoomModel, RoomsActivity.RoomViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull RoomsActivity.RoomViewHolder roomViewHolder, int i, @NonNull RoomModel roomModel)
            {
                roomViewHolder.roomName.setText(roomModel.getRoomName());

                final String rName = roomModel.getRoomName();
                roomViewHolder.btnRoom.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent devicesIntent = new Intent(getApplicationContext(),DevicesActivity.class);
                        devicesIntent.putExtra("HOME_NAME",homeName);
                        devicesIntent.putExtra("ROOM_NAME",rName);
                        startActivity(devicesIntent);
                    }
                });

                roomViewHolder.btnRemove.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        roomsRef.child(rName).removeValue();
                    }
                });
            }

            @NonNull
            @Override
            public RoomsActivity.RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rooms_view_holder,parent,false);
                RoomsActivity.RoomViewHolder viewHolder = new RoomsActivity.RoomViewHolder(view);
                return viewHolder;
            }
        };

        listRooms.setAdapter(adapter);
    }


    public static class RoomViewHolder extends RecyclerView.ViewHolder
    {
        TextView roomName;
        ImageButton btnRoom;
        ImageButton btnRemove;


        public RoomViewHolder(View itemView)
        {
            super(itemView);
            roomName = itemView.findViewById(R.id.txtRoomNameR);
            btnRoom = itemView.findViewById(R.id.btnRoomR);
            btnRemove = itemView.findViewById(R.id.btnRemoveR);
        }

    }

    public void checkPermission()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }


        }
    }


    public void checkVoiceCommand()
    {
        String[] stringArray;
        final String on_off;
        String deviceCommand = "";
        String roomCommand = "";

        stringArray = speechCommand.split(" ");
        on_off = stringArray[0] + " " + stringArray[1];

        if(on_off.equals("TURN ON") || on_off.equals("TURN OFF"))
        {
            int deviceIndex;
            if(on_off.equals("TURN ON"))
                deviceIndex = 8;
            else
                deviceIndex = 9;

            if(speechCommand.substring(deviceIndex, deviceIndex + 3).equals("THE"))
                deviceIndex += 4;

            int indexRoom = StringUtils.indexOf(speechCommand," IN");

            deviceCommand = speechCommand.substring(deviceIndex, indexRoom);

            if(speechCommand.substring(indexRoom+4,indexRoom+7).equals("THE"))
            {
                indexRoom += 8;
                roomCommand = speechCommand.substring(indexRoom);
            }
            else
            {
                roomCommand = speechCommand.substring(indexRoom+4);
            }



            final String dc = deviceCommand;
            roomSpeechRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes").child(homeName).child("rooms").child(roomCommand);

            roomSpeechRef.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.child("roomName").getValue() != null)
                    {
                        if(dataSnapshot.child("devices").child(dc).child("deviceName").getValue() != null)
                        {
                            if (on_off.equals("TURN OFF"))
                                roomSpeechRef.child("devices").child(dc).child("status").setValue("OFF");
                            else
                                roomSpeechRef.child("devices").child(dc).child("status").setValue("ON");
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Unknown device", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Unknown room", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });



        }
        else
        {
            Toast.makeText(getApplicationContext(),"Unknown command", Toast.LENGTH_SHORT).show();
        }
    }

}
