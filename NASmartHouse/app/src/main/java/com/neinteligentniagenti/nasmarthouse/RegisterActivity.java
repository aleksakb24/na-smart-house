package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity
{
    Button btnRegister;
    EditText txtName,txtEmail,txtPassword,txtPassword2;
    ProgressDialog mDialog;
    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnRegister = findViewById(R.id.btnRegisterR);
        txtName = findViewById(R.id.txtNameR);
        txtEmail = findViewById(R.id.txtEmailR);
        txtPassword = findViewById(R.id.txtPasswordR);
        txtPassword2 = findViewById(R.id.txtConfirmPasswordR);

        mAuth = FirebaseAuth.getInstance();

        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                registerUser();
            }
        });
    }

    public void registerUser()
    {
        mDialog = new ProgressDialog(RegisterActivity.this);
        mDialog.setMessage("Please wait...");
        mDialog.show();

        final String name = txtName.getText().toString().trim();
        final String email = txtEmail.getText().toString().trim();
        final String password = txtPassword.getText().toString().trim();
        final String password2 = txtPassword2.getText().toString().trim();

        if(checkInputError(name,email,password,password2))
        {
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
            {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {
                    mDialog.dismiss();

                    if(task.isSuccessful())
                    {
                        Toast.makeText(getApplicationContext(), "Successful registration!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (task.getException() instanceof FirebaseAuthUserCollisionException)
                            Toast.makeText(getApplicationContext(), "A User with this email already exists.", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getApplicationContext(), "There was an error, try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
            {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {

                    if (task.isSuccessful())
                    {
                        addName(name);

                    }
                    else
                        Toast.makeText(getApplicationContext(), "There was an error, try again later.", Toast.LENGTH_SHORT).show();

                }
            });
        }
        else
            mDialog.dismiss();
    }

    public boolean checkInputError(String name, String email,String password,String password2)
    {
        if(name.isEmpty())
        {
            txtName.setError("Please enter a name.");
            txtName.requestFocus();
            return false;
        }

        if(email.isEmpty())
        {
            txtEmail.setError("Please enter an Email.");
            txtEmail.requestFocus();
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            txtEmail.setError("Please enter a correct Email address.");
            txtEmail.requestFocus();
            return false;
        }

        if (password.isEmpty())
        {
            txtPassword.setError("Please enter a password.");
            txtPassword.requestFocus();
            return false;
        }

        if(password.length() < 6)
        {
            txtPassword.setError("Password must be at least 6 characters long.");
            txtPassword.requestFocus();
            return false;
        }

        if (password2.isEmpty())
        {
            txtPassword2.setError("Please enter your password.");
            txtPassword2.requestFocus();
            return false;
        }

        if(!password.equals(password2))
        {
            txtPassword2.setError("Passwords must match.");
            txtPassword2.requestFocus();
            return false;
        }

        return true;
    }

    public void addName(String name)
    {
        if(mAuth.getInstance() != null)
        {
            user = mAuth.getCurrentUser();
            userID = user.getUid();
            mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(userID);
            HashMap userMap = new HashMap();
            userMap.put("name", name);
            userMap.put("uid",userID);
            mDatabase.updateChildren(userMap);

            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setDisplayName(name)
                    .build();

            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>()
            {
                @Override
                public void onComplete(@NonNull Task<Void> task)
                {
                    toIndex();
                }
            });
        }
    }

    public void toIndex()
    {
        Intent loginIntent = new Intent(getApplicationContext(), IndexActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        //this.overridePendingTransition(0, 0);
        finish();
    }
}
