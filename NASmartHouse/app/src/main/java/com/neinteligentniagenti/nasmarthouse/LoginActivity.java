package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity
{

    Button btnLogin;
    EditText txtEmail,txtPassword;
    FirebaseAuth mAuth;
    FirebaseUser user;
    DatabaseReference mDatabase;
    ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btnLoginL);
        txtEmail = findViewById(R.id.txtEmailL);
        txtPassword = findViewById(R.id.txtPasswordL);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users");

        btnLogin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                userLogin();
            }
        });

    }


    public void userLogin()
    {

        mDialog = new ProgressDialog(LoginActivity.this);
        mDialog.setMessage("Please wait...");
        mDialog.show();

        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        if(checkInputError(email,password)) {
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
            {

                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {

                    mDialog.dismiss();

                    if(task.isSuccessful())
                    {
                        Toast.makeText(getApplicationContext(),"Successful login!",Toast.LENGTH_SHORT).show();
                        finish();
                        toIndex();
                    }
                    else
                        Toast.makeText(getApplicationContext(),"Error while logging in.",Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
            mDialog.dismiss();
    }


    public boolean checkInputError(String email,String password)
    {
        if(email.isEmpty())
        {
            txtEmail.setError("Please enter an Email.");
            txtEmail.requestFocus();
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            txtEmail.setError("Please enter a correct Email address.");
            txtEmail.requestFocus();
            return false;
        }

        if (password.isEmpty())
        {
            txtPassword.setError("Please enter a password.");
            txtPassword.requestFocus();
            return false;
        }

        return true;
    }

    public void toIndex()
    {
        Intent loginIntent = new Intent(getApplicationContext(), IndexActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        //this.overridePendingTransition(0, 0);
        finish();
    }
}
