package com.neinteligentniagenti.nasmarthouse;

public class RoomModel
{
    public String roomName;

    public RoomModel()
    {
    }

    public RoomModel(String roomName)
    {
        this.roomName = roomName;
    }

    public String getRoomName() {return roomName;}
    public void setRoomName(String roomName) {this.roomName = roomName;}

}
