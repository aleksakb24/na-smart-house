package com.neinteligentniagenti.nasmarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Locale;


public class IndexActivity extends AppCompatActivity
{

    TextView txtWelcome;
    RecyclerView homesList;
    FloatingActionButton btnAdd, btnSpeech, btnSignout;
    EditText txtDialogHomeName, txtDialogAddress;
    Button btnDialog;

    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    String speechCommand;
    AlertDialog dialog2;

    FirebaseAuth mAuth;
    FirebaseUser user;
    String userID;
    String dialogHomeName;
    String dialogAddress;
    DatabaseReference userRef, homesRef, speechRef, homeSpeechRef;
    FirebaseRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        checkPermission();

        txtWelcome = findViewById(R.id.txtWelcome);
        homesList = findViewById(R.id.rcvHomes);
        btnAdd = findViewById(R.id.fbtnAddHomes);
        btnSpeech = findViewById(R.id.fbtnSpeechI);
        btnSignout = findViewById(R.id.fbtnSignOutI);


        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener()
        {
            @Override
            public void onReadyForSpeech(Bundle params)
            {

            }

            @Override
            public void onBeginningOfSpeech()
            {

            }

            @Override
            public void onRmsChanged(float rmsdB)
            {

            }

            @Override
            public void onBufferReceived(byte[] buffer)
            {

            }

            @Override
            public void onEndOfSpeech()
            {
                mSpeechRecognizer.stopListening();
                dialog2.cancel();

            }

            @Override
            public void onError(int error)
            {

            }

            @Override
            public void onResults(Bundle results)
            {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                if(matches != null)
                {
                    speechCommand = matches.get(0).toUpperCase();
                    speechRef = FirebaseDatabase.getInstance().getReference().child("speech");
                    speechRef.setValue(speechCommand);
                    checkVoiceCommand();
                }

            }

            @Override
            public void onPartialResults(Bundle partialResults)
            {

            }

            @Override
            public void onEvent(int eventType, Bundle params)
            {

            }
        });


        mAuth = FirebaseAuth.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference().child("users");

        if(mAuth.getCurrentUser() != null)
        {
            user = mAuth.getCurrentUser();
            userID = user.getUid();
            String name = user.getDisplayName();
            homesRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes");

            txtWelcome.setText("Welcome, " + name);


            btnAdd.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(IndexActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.add_homes_dialog,null);
                    txtDialogHomeName = mView.findViewById(R.id.txtDialogHomeName);
                    txtDialogAddress = mView.findViewById(R.id.txtDialogAddress);
                    btnDialog = mView.findViewById(R.id.btnDialogAddHome);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    btnDialog.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            dialogHomeName = txtDialogHomeName.getText().toString().toUpperCase().trim();
                            dialogAddress = txtDialogAddress.getText().toString();

                            if(dialogHomeName.isEmpty())
                            {
                                txtDialogHomeName.setError("Please enter a home name.");
                                txtDialogHomeName.requestFocus();
                            }
                            else if(dialogAddress.isEmpty())
                            {
                                txtDialogAddress.setError("Please enter a home name.");
                                txtDialogAddress.requestFocus();
                            }
                            else
                            {
                                homesRef.child(dialogHomeName).child("homeName").setValue(dialogHomeName);
                                homesRef.child(dialogHomeName).child("address").setValue(dialogAddress).addOnSuccessListener(new OnSuccessListener<Void>()
                                {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {
                                        Toast.makeText(getApplicationContext(),"Home added successfully", Toast.LENGTH_SHORT).show();
                                        dialog.cancel();
                                    }
                                });

                            }


                        }
                    });

                }
            });



            btnSpeech.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(IndexActivity.this);
                    View mView2 = getLayoutInflater().inflate(R.layout.speech_recording_home,null);

                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

                    mBuilder2.setView(mView2);
                    dialog2 = mBuilder2.create();
                    dialog2.show();

                }
            });


            btnSignout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    mAuth.signOut();
                    Intent i = new Intent(getApplicationContext(),Main2Activity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);
                    finish();
                }
            });

        }

        linearLayoutManager = new LinearLayoutManager(this);
        homesList.setLayoutManager(linearLayoutManager);
        homesList.setHasFixedSize(true);
        ShowHomes();

    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    public void ShowHomes()
    {
        Query searchHomes = homesRef;

        FirebaseRecyclerOptions<HomeModel> options = new FirebaseRecyclerOptions.Builder<HomeModel>()
                .setQuery(searchHomes, new SnapshotParser<HomeModel>()
                {
                    @NonNull
                    @Override
                    public HomeModel parseSnapshot(@NonNull DataSnapshot snapshot)
                    {
                        return new HomeModel(snapshot.child("homeName").getValue().toString(),snapshot.child("address").getValue().toString());
                    }
                })
                .build();


        adapter = new FirebaseRecyclerAdapter<HomeModel, HomeViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull HomeViewHolder homeViewHolder, int i, @NonNull HomeModel homeModel)
            {
                homeViewHolder.name.setText(homeModel.getHomeName());
                homeViewHolder.address.setText(homeModel.getAddress());

                final String hName = homeModel.getHomeName();
                homeViewHolder.btnHome.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent roomsIntent = new Intent(getApplicationContext(),RoomsActivity.class);
                        roomsIntent.putExtra("HOME_NAME",hName);
                        startActivity(roomsIntent);
                    }
                });

                homeViewHolder.btnRemove.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        homesRef.child(hName).removeValue();
                    }
                });
            }

            @NonNull
            @Override
            public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homes_view_holder,parent,false);
                HomeViewHolder viewHolder = new HomeViewHolder(view);
                return viewHolder;
            }
        };

        homesList.setAdapter(adapter);
    }


    public static class HomeViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView address;
        ImageButton btnHome;
        ImageButton btnRemove;


        public HomeViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.txtHomeNameI);
            address = itemView.findViewById(R.id.txtHomeAddressI);
            btnHome = itemView.findViewById(R.id.btnHomeI);
            btnRemove = itemView.findViewById(R.id.btnRemoveH);
        }

    }


    public void checkPermission()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }


        }
    }

    public void checkVoiceCommand()
    {
        String[] stringArray;
        final String on_off;
        String deviceCommand = "";
        String roomCommand = "";
        String homeCommand = "";

        stringArray = speechCommand.split(" ");
        on_off = stringArray[0] + " " + stringArray[1];

        if(on_off.equals("TURN ON") || on_off.equals("TURN OFF"))
        {
            int deviceIndex;
            if(on_off.equals("TURN ON"))
                deviceIndex = 8;
            else
                deviceIndex = 9;

            if(speechCommand.substring(deviceIndex, deviceIndex + 3).equals("THE"))
                deviceIndex += 4;

            int indexRoom = StringUtils.indexOf(speechCommand," IN");

            deviceCommand = speechCommand.substring(deviceIndex,indexRoom);

            String substringHome = "";

            if(speechCommand.substring(indexRoom+4,indexRoom+7).equals("THE"))
                substringHome = speechCommand.substring(indexRoom+8);
            else
                substringHome = speechCommand.substring(indexRoom+4);

            int indexHome = StringUtils.indexOf(substringHome," IN");

            roomCommand = substringHome.substring(0, indexHome);

            if(substringHome.substring(indexHome+4,indexHome+7).equals("THE"))
                indexHome += 8;
            else
                indexHome += 4;


            homeCommand = substringHome.substring(indexHome);

            final String dc = deviceCommand;
            final String rc = roomCommand;
            final String hc = homeCommand;
            homeSpeechRef = FirebaseDatabase.getInstance().getReference().child("users").child(userID).child("homes");

            homeSpeechRef.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                {
                    if(dataSnapshot.child(hc).child("homeName").getValue() != null)
                    {
                        if(dataSnapshot.child(hc).child("rooms").child(rc).child("roomName").getValue() != null)
                        {
                            if (dataSnapshot.child(hc).child("rooms").child(rc).child("devices").child(dc).child("deviceName").getValue() != null)
                            {
                                if (on_off.equals("TURN OFF"))
                                    homeSpeechRef.child(hc).child("rooms").child(rc).child("devices").child(dc).child("status").setValue("OFF");
                                else
                                    homeSpeechRef.child(hc).child("rooms").child(rc).child("devices").child(dc).child("status").setValue("ON");
                            } else
                            {
                                Toast.makeText(getApplicationContext(), "Unknown device", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Unknown room", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Unknown home", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError)
                {

                }
            });



        }
        else
        {
            Toast.makeText(getApplicationContext(),"Unknown command", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure you want to exit?");
        builder.setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent exit = new Intent(Intent.ACTION_MAIN);
                exit.addCategory(Intent.CATEGORY_HOME);
                exit.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(exit);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }

        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
