package com.neinteligentniagenti.nasmarthouse;

public class DeviceModel
{

    public String deviceName;
    public String status;
    public String type;

    public DeviceModel()
    {
    }

    public DeviceModel(String deviceName, String status, String type)
    {
        this.deviceName = deviceName;
        this.status = status;
        this.type = type;
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }


}
